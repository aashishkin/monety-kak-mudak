﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Монеты
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0, i = 0, orel = 0, reshka = 0, temp = 0; start: Console.WriteLine("Кол-во монет"); if (!int.TryParse(Console.ReadLine(), out n)) {Console.Clear(); Console.WriteLine("Попробуй еще раз"); goto start;} do { Console.WriteLine("1 = решка    0 = орел"); temp = int.Parse(Console.ReadLine()); if (temp == 0) orel++; else if (temp == 1) reshka++; else Console.WriteLine("Ребро?"); i++; } while (i < n); if (orel > reshka) i = reshka; else i = orel; Console.WriteLine($"Переверни {i} монет"); Console.ReadKey();
        }
    }
}
